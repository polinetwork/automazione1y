#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define CRITERIO 11
#define LIMITE 2
#define NMAX 50
#define START '0'
#define END '9'

int valido (char []);
void sommaCifre (int [], int);

int main (int argc, char * argv[])
{
	char isbn [NMAX + 1];
	int ris;

	scanf ("%s", isbn);

	ris = valido (isbn);

	printf ("%d\n", ris);

	return 0;
}

int valido (char isbn [])
{
	int i, j, ris, dim;
	int *seq;

	dim = 0;
	for (i = 0; isbn [i] != '\0'; i++) {
		if (isbn [i] >= START && isbn [i] <= END) {
			dim++;
		}
	}
	if ((seq = malloc (dim * sizeof (int)))) {
		j = 0;
		for (i = 0; isbn [i] != '\0'; i++) {
			if (isbn [i] >= START && isbn [i] <= END) {
				*(seq + j) = isbn[i] - START;
				j++;
			}
		}
		for (i = 0; i < LIMITE; i++) {
			sommaCifre (seq, dim);
		}
		if ((*(seq + (dim - 1)) % CRITERIO) == 0 && *(seq + (dim - 1)) != 0) {
			ris = 1;
		} else {
			ris = 0;
		}
		free (seq);
	} else {
		ris = -1;
		printf ("Errore allocazione memoria per %d valori\n", dim);
	}
	return ris;
}

void sommaCifre (int seq[], int dim)
{
	int i;
	
	for (i = 0; i < dim; i++) {
		seq [i] = seq [i - 1] + seq [i];
	}

	return;
}
