#include <stdio.h>
#include <stdlib.h>
#include "list_char.h"
#define CMAX 50
#define TONDA_APERTA '('
#define TONDA_CHIUSA ')'
#define QUADRA_APERTA '['
#define QUADRA_CHIUSA ']'
#define GRAFFA_APERTA '{'
#define GRAFFA_CHIUSA '}'

int valida (char []);
int valida2 (lista_t *, char);

int main (int argc, char * argv[])
{
	char seq [CMAX+1];
	int ris, annulla, i;

	gets (seq);

	annulla = 0;
	for (i = 0; seq[i] != '\0' && !annulla; i++) {
		if ((seq[i] == TONDA_APERTA && (seq[i+1] == QUADRA_CHIUSA || seq[i+1] == GRAFFA_CHIUSA)) ||
			(seq[i] == QUADRA_APERTA && (seq[i+1] == TONDA_CHIUSA || seq[i+1] == GRAFFA_CHIUSA)) ||
			(seq[i] == GRAFFA_APERTA && (seq[i+1] == TONDA_CHIUSA || seq[i+1] == QUADRA_CHIUSA))) {
			annulla++;
		}
	}

	if (!annulla) {
		ris = valida (seq);
	} else {
		ris = 0;
	}

	printf ("%d\n", ris);
	
	return 0;
}

int valida (char seq[])
{
	lista_t *tonde = NULL, *quadre = NULL, *graffe = NULL;
	int ris, i, ris_tonde, ris_quadre, ris_graffe;

	for (i = 0; seq[i] != '\0'; i++) {
		if (seq[i] == TONDA_APERTA || seq[i] == TONDA_CHIUSA) {
			tonde = append (tonde, seq[i]);
		} else if (seq[i] == QUADRA_APERTA || seq[i] == QUADRA_CHIUSA) {
			quadre = append (quadre, seq[i]);
		} else if (seq[i] == GRAFFA_APERTA || seq[i] == GRAFFA_CHIUSA) {
			graffe = append (graffe, seq[i]);
		}
	}

	ris_tonde = valida2 (tonde, TONDA_APERTA);
	ris_quadre = valida2 (quadre, QUADRA_APERTA);
	ris_graffe = valida2 (graffe, GRAFFA_APERTA);

	if (ris_tonde && ris_quadre && ris_graffe) {
		ris = 1;
	} else {
		ris = 0;
	}

	tonde = empty (tonde);
	quadre = empty (quadre);
	graffe = empty (graffe);

	return ris;
}

int valida2 (lista_t *head, char open)
{
	lista_t *ptr;
	int annidamento, ris;

	annidamento = 0;
	ptr = head;
	while (ptr && annidamento >= 0) {
		if (ptr->val == open) {
			annidamento++;
		} else {
			annidamento--;
		}
		ptr = ptr->next;
	}
	
	if (!annidamento) {
		ris = 1;
	} else {
		ris = 0;
	}

	return ris;
}
