/* Acquisisce un valore, calcola e visualizza 1 se è pari, 0 se è dispari*/

#include <stdio.h>

int main(int argc, char * argv[])
{
	int val, ris;
	/*int unused;*/
	/*Certe volte il compilatore dà warning perché la scanf restituisce un risultato che indica se ha avuto successo o no, salvarlo in una variabile o usare -Wno-unused-result risolve*/
	/*unused = */scanf("%d", &val);

	if((val % 2) == 0) {
		ris = 1;
	} else {
		ris = 0;
	}
	printf("%d\n", ris);
	return 0;
}
