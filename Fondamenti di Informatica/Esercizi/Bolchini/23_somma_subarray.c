/* Somma i valori di un array tra l'indice minimo e massimo dati */
#include <stdio.h>
#define STOP 0
#define DIM 50

int main(int argc, char * argv[])
{
	int mat[DIM];
	int i, ris, dim, tmp, indiceMinimo, indiceMassimo;
	scanf ("%d", &tmp);
	dim = 0;
	while (tmp > STOP && dim < DIM) {
		mat[dim] = tmp;
		dim++;
		scanf ("%d", &tmp);
	}
	
	if (dim > 0) {
		printf ("Ora gli indici minimo e massimo");
		do {
			scanf("%d", &indiceMinimo);
			scanf("%d", &indiceMassimo);
		} while (indiceMassimo < indiceMinimo || indiceMinimo < 0 || indiceMassimo >= dim);
		ris = 0;
		for (i = indiceMinimo; i <= indiceMassimo; i++) {
			ris = ris + mat[i];
		}
	} else {
		ris = 0;
	}
	printf ("%d\n", ris);
	return 0;
}
