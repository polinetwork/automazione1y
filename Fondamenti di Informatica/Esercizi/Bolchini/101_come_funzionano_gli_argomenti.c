/* Mostra il funzionamento degli argomenti */
#include <stdio.h>

int main (int argc, char * argv[])
{
	char *ptr;
	int i, instr;

	/* Per ogni argomento */
	for (i = 0; i < argc; i++) {
		/* Dove inizia la stringa di argv[i] */
		ptr = argv [i];								/* Qui potevo scrivere di nuovo ptr, e prima argv [i] e non usare mai ptr */
		printf ("%p\t%c\t%s\t\targv[%d]\n", ptr, *ptr, argv[i], i);
										/*indirizzo, contenuto, contenuto di tutta la stringa, numero di argomento*/
		/* Indirizzi di tutti i caratteri della stringa */
		for (instr = 1; *(ptr + instr) != '\0'; instr++) {
			printf ("%p\t%c\n", (ptr + instr), *(ptr + instr));
		}
		printf ("%p\t%c\n", (ptr + instr), *(ptr + instr));
		/* Questa stampa sempre spazi vuoti, che sono effettivamente gli spazi che hai
		messo tra gli argomenti, ma lui li interpreta ANCHE come \0 */
	}

	return 0;
}