/*do while serve per i cicli a condizione finale*/
/*Inseire un valore positivo e finché non è tale lo richiede. Poi dire 1 se è pari e 0 se è dispari*/
#include <stdio.h>

int main(int argc, char * argv[])
{
	int val, ris;
	/*scanf("%d", &val);
	while(val < 0) {
		scanf("%d", &val);
	}*/
	do {
		scanf("%d", &val);
	} while(val < 0); /*qui ci va il ;*/
	if (val % 2 == 0) {
		ris = 0;
	} else {
		ris = 1;
	}
	printf("%d\n", ris);
	return 0;
}