// acquisire un valore intero (tempo in secondi), calcola e visualizza il tempo in ore, minuti e secondi
#include <stdio.h>

#define MIN_IN_ORA 60 // queste cose vanno maiuscole e sono immutabili, avrei potuto metterle là sotto al posto di 3600 mettevo queste due moltiplicate e sarebbe stato più chiaro
#define SEC_IN_MIN 60

int main(int argc, char * argv[])
{
// variabili
	int tempo;
	int ore;
	int minuti;
	int secondi;
	int tmp;
// acquisizione
	scanf ("%d", &tempo);
// elaborazione
	ore = tempo / 3600;
	tmp = tempo % 3600;
	minuti = tmp / 60;
	secondi = tmp % 60;
// display
	printf("%d ore %d minuti e %d secondi\n", ore, minuti, secondi); // \n dice di andare a capo, le parole tra i %d vengono stampate così come sono tra le variabili da rappresentare, ma negli esercizi non vanno messe
	return 0;
}