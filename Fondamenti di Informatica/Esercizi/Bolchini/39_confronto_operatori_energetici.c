#include <stdio.h>
#define CMAX 50
#define NOP 10

typedef struct operatore_s {
	char nome[CMAX+1];
	float prezzoGas, prezzoKw;
} operatore_t;

int main(int argc, char * argv[])
{
	operatore_t input[NOP];
	float stimaKw, stimaGas, costoMinimo, costo;
	int i, tmp, validi, willInsert, ris;

	validi = 0;
	do {
		printf("Inserisci il nome dell'operatore\n");
		scanf("%s", input[validi].nome);
		printf("Inserisci il prezzo della componente energia elettrica in euro/KWh\n");
		do {
			scanf("%f", &input[validi].prezzoKw);
		} while (input[validi].prezzoKw < 0);
		printf("Inserisci il prezzo della componente gas naturale in euro/mc\n");
		do {
			scanf("%f", &input[validi].prezzoGas);
		} while (input[validi].prezzoGas < 0);
		validi++;
		if (validi > 1) {
			printf("Vuoi inserire un altro operatore da confrontare? [0/1]\n");
			scanf("%d", &willInsert);
		} else {
			willInsert = 1;
		}
	} while (willInsert && validi < NOP);
	do {
		printf("Inserisci il tuo consumo medio annuale di energia elettrica\n");
		do {
			scanf("%f", &stimaKw);
		} while (stimaKw < 0);
		printf("Inserisci il tuo consumo medio annuale di gas naturale\n");
		do {
			scanf("%f", &stimaGas);
		} while (stimaGas < 0);
	} while (stimaKw == 0 && stimaGas == 0); /* Singolarmente le puoi mettere a 0 ma entrambe non ha senso */
	
	ris = 0;
	costoMinimo = (input[ris].prezzoKw * stimaKw) + (input[ris].prezzoGas * stimaGas);
	for (i = 1; i < validi; i++) {
		costo = (input[i].prezzoKw * stimaKw) + (input[i].prezzoGas * stimaGas);
		if (costo < costoMinimo) {
			ris = i;
			costoMinimo = costo;
		}
	}

	printf("L'operatore migliore per te e'\n");
	printf("%s\n", input[ris].nome);
	return 0;
}