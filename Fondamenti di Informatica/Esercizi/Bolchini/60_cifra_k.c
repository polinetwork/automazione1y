#include <stdio.h>
#define BASE 10
int cifra (int, int);

int main(int argc, char * argv[])
{
	int num, k, ris;

	scanf("%d", &num);
	scanf("%d", &k);

	if (k > 0) {
		ris = cifra (num, k);
	} else {
		ris = -1;
	}

	printf("%d\n", ris);
	return 0;
}

int cifra (int num, int k)
{
	int i, div, ris;

	div = num;
	for (i = 0; i < k - 1 && div != 0; i++) {
		div = div / BASE;
	}
	if (div != 0) {
		ris = div % BASE;
		if (ris < 0) {
			ris = -ris;
		}
	} else {
		ris = -1;
	}
	
	return ris;
}