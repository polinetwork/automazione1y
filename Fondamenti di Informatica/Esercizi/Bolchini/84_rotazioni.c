/* Scrivere un programma che riceve in ingresso un numero intero positivo (e se non è tale lo richiede) e
visualizza il valore intero più grande che si può ottenere mediante una o più rotazioni a destra e una o più
rotazioni a sinistra. Una rotazione a destra è quando metti all'inizio la cifra meno significativa, una a sinistra è quando metti alla fine la più significativa. */
#include <stdio.h>
#include <math.h>
#define BASE 10

int contaCifre (int);
int ruotaDx (int, int*);
int ruotaSx (int, int*);

int main (int argc, char * argv[])
{
	int value, maxValue, tmp, cifre, i, tmpCifre;
	
	do {
		scanf ("%d", &value);
	} while (value <= 0);

	cifre = contaCifre (value);

	maxValue = value;
	tmp = value;
	tmpCifre = cifre;
	for (i = 0; i < cifre - 1 && cifre == tmpCifre; i++) {
		tmp = ruotaDx (tmp, &tmpCifre);
		if (tmp > maxValue) {
			maxValue = tmp;
		}
	}

	tmp = value;
	for (i = 0; i < cifre - 1; i++) {
		tmp = ruotaSx (tmp, &tmpCifre);
		if (tmp > maxValue) {
			maxValue = tmp;
		}
	}

	printf ("%d\n", maxValue);
	return 0;
}

int contaCifre (int val)
{
	int cifre;
	
	for (cifre = 0; val > 0; cifre++) {
		val = val / BASE;
	}

	return cifre;
}

int ruotaDx (int val, int *dim)
{
	int cifra, ris;

	cifra = val % BASE;

	ris = (cifra * ((int) (pow (BASE, (*dim-1))))) + (val / BASE);

	*dim = contaCifre (ris);

	return ris;
}

int ruotaSx (int val, int *cifre)
{
	int cifra, ris, dim, bkp;

	bkp = val;
	cifra = 0;
	for (dim = 0; bkp > 0; dim++) {
		cifra = bkp % BASE;
		bkp = bkp / BASE;
	}

	ris = (val % ((int) (pow (BASE, (dim-1))))) * BASE + cifra;

	*cifre = contaCifre (ris);

	return ris;
}
