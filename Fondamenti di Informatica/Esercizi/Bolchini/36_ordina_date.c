/* Struct per date */
#include <stdio.h>
#define MAXETICHETTA 10

typedef struct date_s {
		int giorni, mesi, anni;
} date_t;

/* Per le coordinate più etichetta */
typedef struct coordinate_s {
		int x, y;
		char etichetta[MAXETICHETTA];
} coordinate_t;

int main(int argc, char * argv[])
{
	date_t datainizio, datafine;
	/* L'accesso è sempre una cosa per volta */
	scanf("%d%d%d", &datainizio.giorni, &datainizio.mesi, &datainizio.anni);
	scanf("%d%d%d", &datafine.giorni, &datafine.mesi, &datafine.anni);
	printf("\n");
	if (datafine.anni < datainizio.anni) {
		printf("%d %d %d\n", datafine.giorni, datafine.mesi, datafine.anni);
		printf("%d %d %d\n", datainizio.giorni, datainizio.mesi, datainizio.anni);
	} else if (datafine.anni > datainizio.anni) {
		printf("%d %d %d\n", datainizio.giorni, datainizio.mesi, datainizio.anni);
		printf("%d %d %d\n", datafine.giorni, datafine.mesi, datafine.anni);
	} else {
		if (datafine.mesi < datainizio.mesi) {
			printf("%d %d %d\n", datafine.giorni, datafine.mesi, datafine.anni);
			printf("%d %d %d\n", datainizio.giorni, datainizio.mesi, datainizio.anni);
		} else if (datafine.mesi > datainizio.mesi) {
			printf("%d %d %d\n", datainizio.giorni, datainizio.mesi, datainizio.anni);
			printf("%d %d %d\n", datafine.giorni, datafine.mesi, datafine.anni);
		} else {
			if (datafine.giorni < datainizio.giorni) {
				printf("%d %d %d\n", datafine.giorni, datafine.mesi, datafine.anni);
				printf("%d %d %d\n", datainizio.giorni, datainizio.mesi, datainizio.anni);
			} else if (datafine.giorni > datainizio.giorni) {
				printf("%d %d %d\n", datainizio.giorni, datainizio.mesi, datainizio.anni);
				printf("%d %d %d\n", datafine.giorni, datafine.mesi, datafine.anni);
			}
		}
	}
	return 0;
}

/* Per fare in modo di salvare tutto in una variabile avrei dovuto dichiarare date_t data1, data2; e poi nelle if fare tipo datainizio.giorno = data1.giorno; */
/* N.B. se chiedo a un if di fare datainizio == datafine non funziona */
/* Come eccezione posso però copiare tutta una struttura in un'altra con un semplice assegnamento data1 = data2; */