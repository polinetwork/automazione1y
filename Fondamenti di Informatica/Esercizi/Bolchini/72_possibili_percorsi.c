/* Gli dai righe e colonne di una matrice e lui ti dice il numero di modi possibili in cui passare dalla prima casella all'ultima in basso a destra muovendosi solo o a destra o in basso */
#include <stdio.h>

int calcolaPercorsi (int, int, int, int);

int main (int argc, char*argv[])
{
	int rows, cols, numeroPercorsi;

	scanf ("%d", &rows);
	scanf ("%d", &cols);

	numeroPercorsi = calcolaPercorsi (0, 0, rows-1, cols-1);

	printf ("%d\n", numeroPercorsi);
	return 0;
}

int calcolaPercorsi (int curR, int curC, int goalR, int goalC)
{
	int ris;

	if (curC > goalC || curR > goalR) { /* Se ho sforato o a destra o sotto questa non è una strada percorribile */
		ris = 0;
	} else if (curR == goalR && curC == goalC) { /* Se sono arrivato questa è l'unica strada */
		ris = 1;
	} else { /* Altrimenti proviamo a spostarci di 1 a destra e contare le strade da lì. Poi bisogna sommarlo alle strade che trovi spostandoti di 1 in basso */
		ris = calcolaPercorsi (curR+1, curC, goalR, goalC) + calcolaPercorsi (curR, curC+1, goalR, goalC);
	}
	
	return ris;
}

