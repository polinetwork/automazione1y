#include <stdio.h>
#define NMAX 25
#define MAXPAROLE 20
#define BLOCKS "*"

typedef struct quadro_s {
	char seq[NMAX+1];
	int len;
} quadro_t;

int main(int argc, char * argv[])
{
	quadro_t io[MAXPAROLE];
	int num, i, j, max;

	do {
		scanf("%d", &num);
	} while (num <= 0);
	for (i = 0; i < num; i++) {
		scanf("%s", io[i].seq);
		io[i].len = 0;
		while (io[i].seq[io[i].len] != '\0') {
			io[i].len++;
		}
	}
	max = io[0].len;
	for (i = 1; i < num; i++) {
		if (io[i].len > max) {
			max = io[i].len;
		}
	}
	for (i = 0; i <= max + 1; i++) {
		printf(BLOCKS);
	}
	printf("\n");
	for (i = 0; i < num; i++) {
		printf(BLOCKS);
		printf("%s", io[i].seq);
		for (j = 0; j < (max - io[i].len); j++) {
			printf(" ");
		}
		printf(BLOCKS);
		printf("\n");
	}
	for (i = 0; i <= max + 1; i++) {
		printf(BLOCKS);
	}
	printf("\n");
	return 0;
}
