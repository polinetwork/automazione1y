#include "list_char.h"
#define CMAX 50
#define TONDA_APERTA '('
#define TONDA_CHIUSA ')'
#define QUADRA_APERTA '['
#define QUADRA_CHIUSA ']'
#define GRAFFA_APERTA '{'
#define GRAFFA_CHIUSA '}'

int valida (char []);

int main (int argc, char * argv[])
{
	char *seq, *nomeExe;
	int ris;

	nomeExe = argv[0];
	if (argc == 2) {
		seq = argv[1];
		ris = valida (seq);
		if (ris) {
			system ("color a");
		} else {
			system ("color c");
		}
		printf ("%d\n", ris);
	} else if (argc == 1) {
		system ("color c");
		printf ("%s: Error: Too few arguments\n", nomeExe);
	} else {
		system ("color c");
		printf ("%s: Error: Too many arguments\n", nomeExe);
	}
	
	return 0;
}

int valida (char str[])
{
	lista_t *stack = NULL;
	int i, valida;
	char p;

	valida = 1;

	for (i = 0; str[i] != '\0' && valida; i++) {
		if (str[i] == TONDA_APERTA || str[i] == QUADRA_APERTA || str[i] == GRAFFA_APERTA) {
			stack = push (stack, str[i]);
		} else {
			if (!stack) {
				valida = 0;
			} else if (str[i] == TONDA_CHIUSA) {
				p = stack->val;
				stack = delPos (stack, 0, SEEK_SET);
				if (p != TONDA_APERTA) {
					valida = 0;
				}
			} else if (str[i] == QUADRA_CHIUSA) {
				p = stack->val;
				stack = delPos (stack, 0, SEEK_SET);
				if (p != QUADRA_APERTA) {
					valida = 0;
				}
			} else if (str[i] == GRAFFA_CHIUSA) {
				p = stack->val;
				stack = delPos (stack, 0, SEEK_SET);
				if (p != GRAFFA_APERTA) {
					valida = 0;
				}
			}
		}
	}

	if (stack) {
		valida = 0;
	}

	stack = empty (stack);

	return valida;
}

