#include <stdio.h>
#include <string.h>
#define CMAX 100

void palindroma (char []/* , int */);

int main(int argc, char * argv[])
{
	char seq[CMAX+1];
	int dim, i;

	scanf(seq);
	/* dim = strlen(seq); */

	ris = palindroma(seq, dim);

	printf("%d\n", ris);
	return 0;
}

int palindroma (char seq[]/* , int dim */); /* Con le stringhe è meglio non passare dim in ingresso (a meno che non serva a più sottoprogrammi e allora la calcolo una volta sola) perché può pure darsi che non serva. Con gli array invece non c'è il terminatore per cui non può non servire */
{
	int i, j, ris;
	
	dim = strlen(seq);
	ris = 1;
	for (i = 0; j--; i < j && ris; i++, j--) {
		if (seq[i[ != seq[j]) {
			ris = 0;
		}
	}

	return ris;
}
