#include <stdio.h>
#include <string.h>
#define CMAX 100
#define	LOINIT 'a'
#define	LOEND 'z'
#define	UPINIT 'A'
#define UPEND 'Z'
#define SPACE ' '

void modifica (char []);

int main(int argc, char * argv[])
{
	char io[CMAX+1];

	gets (io);
	
	modifica (io);

	printf ("%s\n", io);

	return 0;
}

void modifica (char io[])
{
	int pos, i;

	for (i = 0; io[i] != '\0'; i++) {
		while (io[i] == SPACE) {
			i++;
		}
		if (io[i] >= LOINIT && io[i] <= LOEND) {
			pos = io[i] - LOINIT;
			io[i] = UPINIT + pos;
		}
		i++;
		while (io[i] != SPACE && io[i] != '\0') {
			if (io[i] >= UPINIT && io[i] <= UPEND) {
				pos = io[i] - UPINIT;
				io[i] = LOINIT + pos;
			}
			i++;
		}
	}

	return;
}
