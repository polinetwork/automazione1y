/* Gli dai una lista di prezzi, lui li ordina in maniera crescente e ti dice quanti ne puoi comprare con un budget */
#include <stdio.h>
#define NMAX 50
#define STOP 0

int main(int argc, char * argv[])
{
    float val[NMAX], prezzo, costo_totale, max, tmp;
    int i, j, dim, num_pezzi, minPos;
    
    dim = 0;
    scanf("%f", &prezzo);
    while(prezzo >= STOP && dim < NMAX){
        val[dim] = prezzo;
        dim++;
        scanf("%f", &prezzo);
    }
	do {
		printf("Ora il valore massimo maggiore o uguale a zero\n");
		scanf("%f", &max);
	} while (max < 0);
	
	/* Come ordinare un array (selection sorting) */
	for (i = 0; i < dim - 1; i++) { /* Tanto è inutile confrontare l'ultimo elemento con altro, tanto tutto quello a sinistra di i è già ordinato */
		minPos = i; /* Suppongo che la posizione del minimo sia inizialmente proprio il valore che sto considerando */
		for (j = i + 1; j < dim; j++) {
			if (val[j] < val[minPos]) { /* La condizione è l'unica cosa che cambia a seconda di quello che voglio farci con l'algoritmo */
				minPos = j;
			}
		}
		tmp = val[i];
		val [i] = val[minPos];
		val [minPos] = tmp;
	}
	costo_totale = 0;
	num_pezzi = 0;
	i = 0;
	while (i < dim && costo_totale <= max) {
		costo_totale += val[i];
		if (costo_totale <= max) {
			num_pezzi++;
		}
		i++;
	}
	if (costo_totale > max) {
		costo_totale -= val[i-1];
	}
    printf("%d: %f\n", num_pezzi, costo_totale);
    return 0;
}
