#include <stdio.h>
#define NMAX 100
#define TERMINATORE_PERSONALIZZATO -666

void min_max (int [], int, int*, int*);

int main(int argc, char * argv[])
{
    int seq[NMAX], dim, minval, maxval, i, tmp;
	
	scanf ("%d", &tmp);
	dim = 0;
	while (tmp != TERMINATORE_PERSONALIZZATO && dim < NMAX) {
		seq[dim] = tmp;
		dim++;
		scanf ("%d", &tmp);
	}

	min_max(seq, dim, &minval, &maxval);
	printf("\n");
	/*printf("%d\n", minval);
	printf("%d\n", maxval);*/
	printf("%d\n", seq[minval]);
	printf("%d\n", seq[maxval]);
	for (i = 0; i < dim; i++) {
		printf("%d ", seq[i]);
	}
	printf("\n");
	return 0;
}

void min_max (int seq[], int dim, int*minval, int*maxval /* Sta roba qua indica una variabile con dentro l'indirizzo di quell'altra (& invece è un operatore che dato il nome esce l'indirizzo) */)
{
	int i, max, min;

	/*min = seq[0];
	max = seq[0];*/
	min = 0;
	max = 0;
	for (i = 0; i < dim /* Qua non c'è il terminatore quindi devo passargli o calcolare il numero di elementi significativi */; i++) {
		if (seq[i] > max) {
			/*max = seq[i];*/
			max = i;
		} else if (seq[i] < min) {
			/*min = seq[i];*/
			min = i;
		}
	}
	*minval = min;
	*maxval = max;

	return;
}
