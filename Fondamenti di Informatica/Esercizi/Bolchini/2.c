// dato un numero calcola e visualizza il valore assoluto senza corrompere val

#include <stdio.h>

int main(int argc, char * argv[])
{
	int val, mod;
	
	scanf("%d", &val);

	if(val < 0) {
		mod = -val; // come dire 0-val
	} else {
		mod = val;
	}
	printf("%d\n", mod); // fare 2 printf dentro l'if funziona ma non ha senso perché la specifica dice "calcola e visualizza"
	return 0;
}
// l'alternativa era salvare val in mod prima dell'if e poi non usare l'else ma la metà delle volte (quando val è negativo) fa il doppio delle istruzioni
// perché viene impostato sbagliato e poi cambiato