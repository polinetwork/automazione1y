/*Stessa cosa del successivo ma con for*/
#include <stdio.h>
#define NUMVAL 20

int main(int argc, char * argv[])
{
	int val[NUMVAL];
	int i, soglia, count;
	for (i = 0; i < NUMVAL; i++) {
		scanf("%d", &val[i]);
	}
	scanf("%d", &soglia);
	count = 0;
	for (i = 0; i < NUMVAL; i++) { /* prima del primo ; posso mettere anche altre inizializzazioni. For si usa SOLO per i cicli a conteggio */
		if (val[i] > soglia) {
			count++;
		}
	}
	printf("%d\n", count);
	return 0;
}