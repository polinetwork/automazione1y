/* Prendo 50 caratteri minuscoli e calcolo e viusualizzo il numero di occorrenze di ciascuno */
#include <stdio.h>
#define NUMVAL 50
#define ALFA 26
#define PRIMO 'a'

int main(int argc, char * argv[])
{
	char val[NUMVAL];
	int count[ALFA]; /* è giusto fare gli array di contatori */
	int i, pos;
	i = 0;
	for (i = 0; i < NUMVAL; i++) {
		scanf("%c", &val[i]); /* L'invio è un carattere quindi i caratteri vanno scritti di seguito e poi un solo invio */
	}
	for (i = 0; i < ALFA; i++) {
		count[i] = 0;
	}
	for (i = 0; i < NUMVAL; i++) { /* c-a = 2 per il codice ASCII */
		pos = val[i] - PRIMO; /* Non c'è bisogno del cast perché è già capace di fare operazioni con i valori ASCII sotto, serve anche oer fare l'ordine alfabetico */
		count[pos] = count[pos] + 1; /* count[pos]++; */
	}
	for (i = 0; i < ALFA; i++) {
		if(count[i] > 0) {
			printf("%c %d\n", PRIMO + i, count[i]); /* PRIMO + i serve perché così stampa una cosa che è i dopo "a" */
		}
	}
	return 0;
}