#include "list_int.h"
#define STOP 0

lista_t * rimuoviprimi (lista_t *);
int isprime (int);

int main (int argc, char * argv [])
{
    lista_t *head = NULL;
    int val;
    
    scanf ("%d", &val);
    while (val != STOP) {
        head = append (head, val);
        scanf ("%d", &val);
    }
    head = rimuoviprimi (head);
    printl (head);
    head = empty (head);

    return 0;
}

lista_t * rimuoviprimi (lista_t *head)
{
    lista_t *ptr, *todelete;
    int i, j;

    while (head && isprime (abs(head->val))) {
        head = delPos (head, 0, SEEK_SET);
    }
    ptr = head;
    for (i = 0; ptr && ptr->next; i++) {
        if (isprime (abs(ptr->next->val))) {
            todelete = ptr->next;
            ptr->next = todelete->next;
            free(todelete);
        } else {
            ptr = ptr->next;
        }
    }

    return head;
}

int isprime (int val)
{
    int primo, div;

    if (val % 2 == 0) {
        primo = 0;
    } else {
        primo = 1;
        for (div = 3; div <= val / 2; div = div + 2) {
            if (val % div == 0) {
                primo = 0;
            }
        }
    }

    return primo;
}