/* ricevuto in ingresso una stringa e un carattere restituice il numero di volte che il carattere compare */

#include <stdio.h>
#define CMAX 100

int occorrenze_r (char [], char);
int esiste_r (char [], char);

int main(int argc, char * argv[])
{
	int ris, esiste;
	char seq[CMAX+1], trg;

	scanf ("%s %c", seq, &trg);
	
	ris = occorrenze_r (seq, trg);
	esiste = esiste_r (seq, trg);

	printf ("%d %d\n", ris, esiste);

	return 0;
}

int occorrenze_r (char s[], char trg)
{
	int count;

	if (s[0] == '\0') {
		count = 0;
	} else {
		if (s[0] == trg) {
			count = 1;
		} else {
			count = 0;
		}
		count = count + occorrenze_r (&s[1], trg);
	}
	
	return count;
}

int esiste_r (char s[], char trg)
{
	int count;

	if (s[0] == '\0') { /* possono esserci anche 2 condizioni che danno risultati diversi ma fanno comunque interrompere la ricorsione */
		count = 0;
	} else if (s[0] == trg) {
		count = 1;
	} else {
		count = occorrenze_r (&s[1], trg);
	}
	
	return count;
}