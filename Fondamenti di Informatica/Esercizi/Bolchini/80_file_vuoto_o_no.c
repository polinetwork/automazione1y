#include <stdio.h>
#include <string.h>
#define CMAX 50

int vuoto (char [], char []);

int main (int argc, char * argv[])
{
	char nome [CMAX+1], percorso [CMAX+1];
	int ris;

	scanf ("%s", nome);
	scanf ("%s", percorso);

	ris = vuoto (nome, percorso);

	printf ("%d\n", ris);

	return 0;
}

int vuoto (char nome [] char percorso[])
{
	FILE *fin;
	int ris, i, dim_nome, dim_percorso, dim;
	char tmp;
	char *fileName;

	dim_percorso = strlen (percorso);
	dim_nome = strlen (nome);
	dim = dim_percorso + dim_nome;
	
	ris = 0;
	if ((fileName = malloc ((dim + 1) * sizeof (char)))) {
		for (i = 0; i < dim_percorso; i++) {
			fileName [i] = percorso [i];
		}
		for (i = 0; i < dim_percorso; i++) {
			fileName [dim_percorso + i] = percorso [i];
		}
		fileName [dim] = '\0';

		if ((fin = fopen (fileName, "r"))) {
			if (fscanf (fin, "%c", &tmp) != EOF) {
				ris = 1;
			}
			fclose (fin);
		} else {
			printf ("Problemi con il file %s\n", fileName);
			ris = -1;
		} 
	} else {
		printf ("Problemi con l'allocazione della memoria per %d caratteri\n", (dim + 1));
		ris = -1;
	}

	return ris;
}