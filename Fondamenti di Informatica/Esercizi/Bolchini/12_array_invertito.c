/*Stampa 5 valori al contrario*/
#include <stdio.h>
#define NUMVAL 5

int main(int argc, char * argv[])
{
	/*int val1, val2, val3, val4, val5;
	scanf("%d", &val1);
	scanf("%d", &val2);
	scanf("%d", &val3);
	scanf("%d", &val4);
	scanf("%d", &val5);
	printf("%d %d %d %d %d", val5, val4, val3, val2, val1;*/
	/* Si chiamano array o vettori. Dati omogenei e la dimensione (quanti dati avrò) deve essere nota, oppure almeno devi sapere il numero massimo e poi al limite ne usi di meno */
	int val[NUMVAL]; /*val[0] è il primo valore e così via*/
	int i; /*indice che assume i valori 0, 1, 2, 3, 4*/
	i = 0;
	while (i < NUMVAL) {
		scanf("%d", &val[i]);
		i++;
	}
	i--; /*valeva 5 alla fine del ciclo ma va fatto ripartire da 4*/
	while (i >= 0) {
		printf("%d", val[i]);
		i--;
	}
	printf("\n");
	return 0;
}