#include <stdio.h>
#define CMAX 100

int strlen_r (char []);

int main(int argc, char * argv[])
{
	int ris;
	char seq[CMAX+1];

	scanf ("%s", seq);

	ris = strlen_r (seq);

	printf ("%d\n", ris);

	return 0;
}

int strlen_r (char s[])
{
	int dim;

	if (s[0] == '\0') {
		dim = 0;
	} else {
		dim = 1 + strlen_r(&s[1]); /* adesso al chiamato s[1] apparirà come s[0]. poi lui richiama di nuovo e passa &s[1] (che nell'originale è s[2]) e al chiamato apparirà sempre come s[0]. sta cosa funziona perché quando passo in questo modo passo l'indirizzo di tutto l'array a partire dal numero tra parentesi (in realtà passo solo l'indirizzo del numero tra parentesi e chi legge legge a partire da lì */
	}
	
	return dim;
}
