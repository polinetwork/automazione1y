#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define OPTIONS 2

int buffa (char []);

int main (int argc, char * argv[])
{
	int ris;
	
	if (argc == OPTIONS) {
		ris = buffa (argv[1]);
		printf ("%d\n", ris);
	} else {
		printf ("Numero di argomenti sbagliato\n");
	}

	return 0;
}

int buffa (char seq[])
{
	int i, j, dim, diff, diff_inv, ris;

	ris = 1;
	dim = strlen (seq) - 1;
	j = dim;
	for (i = 0; i < dim && ris; i++) {
		diff = abs(seq[i + 1] - seq[i]);
		printf ("%c - %c = %d\n", seq[i+1], seq[i], diff);
		diff_inv = abs(seq[j - 1] - seq[j]);
		printf ("%c - %c = %d\n", seq[j-1], seq[j], diff_inv);
		if (diff != diff_inv) {
			ris = 0;
		}
		j--;
	}

	return ris;
}