/* Sottoproigramma verifiche che ricevuta una stringa che contiene una password restituisce 1 se sono verificate le seguenti condizioni, 0 altrimenti: */
/* Almeno una cifra, un carattere speciale, lunghezza minimo 8, non contiene 2 caratteri consecutivi */
#include <stdio.h>
#define CMAX 100
#define NUMMIN '0'
#define NUMMAX '9'
#define	LOINIT 'a'
#define	LOEND 'z'
#define	UPINIT 'A'
#define UPEND 'Z'
#define LMIN 8

int verifica (char []);
int isnotalfa (char);

int main(int argc, char * argv[])
{
	char input[CMAX+1];
	int ris;

	scanf ("%s", input);
	
	ris = verifica (input);

	if (ris) {
		printf ("La password è valida\n");
	} else {
		printf ("La password non è valida\n");
	}

	return 0;
}

int verifica (char seq[])
{
	int isok, i, hacifra, hanonalfa, hadoppia;

	hacifra = 0;
	hanonalfa = 0;
	hadoppia = 0;
	for (i = 0; seq[i] != '\0' && !hadoppia; i++) {
		if (seq[i] >= NUMMIN && seq[i] <= NUMMAX) {
			hacifra++;
		} else if (isnotalfa (seq[i])) {
			hanonalfa++;
		}
		if (seq[i] == seq[i+1]) {
			hadoppia++;
		}
	}
	
	isok = hacifra && hanonalfa && !hadoppia && (i >= LMIN);

	return isok;
}

int isnotalfa (char c)
{
	int ris;
	
	if (!((c >= LOINIT && c <= LOEND) || (c >= UPINIT && c <= UPEND))) {
		ris = 1;
	} else {
		ris = 0;
	}

	return ris;
}
